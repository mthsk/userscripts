# userscripts

A repository meant to host my JavaScript User Scripts.

My scripts are mostly tested on Violentmonkey on Firefox but I do try to maintain compatibility with Tampermonkey, as well as Chromium browsers and GreaseMonkey 3 on Pale Moon.

If you don't already have an userscript manager installed in your browser, I recommend getting [ViolentMonkey](https://violentmonkey.github.io/).

## Support My Work

**Kofi:** https://ko-fi.com/mthsk  
**BTC:** 12oAx9b6YiiTrnnqmbXi2tikrT5a3GcP16  
**ETH:** 0x4F5db2cE69E6b95d642c89E90Bc1d9e0C026C058  
**XMR:** 89fGtdcRrFeA8XSgCzjsttFqQznVBRH768YPCSxZZxhTdJbjmfSHxxec4UB2hWrKS14tBXSTjwQp4W3LLRVj7fC9HQaYV33

### Invidious Local Subscriptions

Implements local subscriptions on Invidious. Should work on any Invidious instance.

#### Installation: [Click here](https://codeberg.org/mthsk/userscripts/raw/branch/master/inv-local-subscriptions/inv-local-subscriptions.user.js), or get it on [GreasyFork](https://greasyfork.org/scripts/457411-invidious-local-subscriptions).

### Invidious Embed

Replaces all YouTube embeds with Invidious ([yewtu.be](https://yewtu.be/)) embeds wherever you go.

#### Installation: [Click here](https://codeberg.org/mthsk/userscripts/raw/branch/master/invidious-embed/invidious-embed.user.js), or get it on [GreasyFork](https://greasyfork.org/scripts/438131-invidious-yewtu-be-embed).

### Rimgo Embed

Replaces all imgur image embeds with Rimgo ([i.bcow.xyz](https://i.bcow.xyz/)) embeds wherever you go.

#### Installation: [Click here](https://codeberg.org/mthsk/userscripts/raw/branch/master/rimgo-embed/rimgo-embed.user.js)

### Simple Libre Redirects

Redirects you from big tech websites to their free-as-in-freedom implementations.

#### Installation: [Click Here](https://codeberg.org/mthsk/userscripts/raw/branch/master/simple-libre-redirects/simple-libre-redirects.user.js)

### Simple Sponsor Skipper

A simple, private and secure userscript implementation of SponsorBlock. Compatible with YouTube, Invidious, CloudTube and Odysee.  
Skips annoying intros, sponsors and w/e using the SponsorBlock API.

Once installed, it can be configured [here](https://www.youtube.com/#s3config).

#### Installation: [Click here](https://codeberg.org/mthsk/userscripts/raw/branch/master/simple-sponsor-skipper/simple-sponsor-skipper.user.js), or get it on [GreasyFork](https://greasyfork.org/scripts/453320-simple-sponsor-skipper).